<img align=center src="https://i.imgur.com/RFIuxyU.jpg" alt="Architectural Style Detector Poster">

## Setup your data, train, test and deploy
I labeled the data using Microsoft's VoTT.

Most of the computationally expensive operations were executed on an EC2 
GPU instance from AWS using a modified version of their anaconda 
environment `tensorflow_p36`. This tutorial presupposes access
to AWS, although the steps can easily be replicated on a local machine.

### Prerequisites
1. Setup an anaconda environment similar to `tensorflow_p36`
2. Have some labeled data, this tutorial includes conversion scripts for VoTT data
3. Create a folder with your project (`mkdir project`)
4. Clone this repo there
5. Setup AWS CLI (https://aws.amazon.com/cli/)

### Dataset cleanup
1. Use `scripts/data-cleanup/vott_to_json.py` to convert VoTT data into a clean
JSON containing all your boxes in relative coordinates. They look like this:
    ~~~json
    [
      {
        "file": "images/archaeminid_31.jpg",
        "width": 800,
        "height": 600,
        "annotations": [
          {
            "score": 1,
            "detectionString": "archaeminid",
            "detectionClass": 3,
            "box": [
              0.1536796537,
              0.0016233766,
              0.4729437229,
              0.9983766234
            ]
          }
        ]
      },
      ...
    ]
    ~~~
2. Use `scripts/data-cleanup/split-dataset.py` to generate train/test/validation
subsets of the JSON created at step 1. In this instance I split 80/10/10 for each 
class. Make sure that the filenames provided in the JSONs correspond to your 
file structure, mine are `images/filename-x.jpg` and my folder structure for
the dataset is:
    ~~~
     ─── dataset
         |─── images
         |    |─── filename-1.jpg
         |    |...
         |   
         |─── train.json
         |─── validation.json
         |─── test.json
    ~~~

3. Convert the images + JSONs to `tfrecord` with `dataset.py` like so:
    ~~~sh
    python dataset.py export_data \
        --export_type=tf_record \
        --annotation_source={file_name}.json \
        --destination=tf_records/{record_name}.record
    ~~~
    
4. Update `tf_config/architecture_label_map.pbtxt` with your class mapping.

### Training
1. Optional. Upload another model to `pretrained_weights`. This repo
uses `faster_rcnn_resnet101_coco`. Find more on https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md

2. Edit (or duplicate) the two configuration files in the `tf_config` folder.

    For both:
    * Specify `num_classes`
    * Change each `label_map_path` to the file created at 4.
    
    For train config:
    *  Change `fine_tune_checkpoint` in `train_config` to point 
    to your downloaded model `model.ckpt` file
    * Change `input_path` from `train_input_reader` to point to your 
    *train* `tfrecord` file
    * Change `input_path` from `eval_input_reader` to point to your
    *validation* `tfrecord` file
    * In `eval_config`: change `max_num_boxes_to_visualize` to how many
    boxes you'd prefer the model to visualize per image, change `num_visualizations`
    to how many different images you'd like to have visualizations for.
    
    For test config:
    * Change `input_path` from `eval_input_reader` to point to your
    *test* `tfrecord` file
    * In `eval_config`: change `max_num_boxes_to_visualize` to how many
    boxes you'd prefer the model to visualize per image, change `num_visualizations`
    to the number of images in your test set (if you'd prefer to see all of them like I did)
    
3. Activate the conda environment (I recommend doing this after
opening a new `screen` window):
    ~~~sh
    source /home/ubuntu/anaconda3/etc/profile.d/conda.sh
    conda activate tensorflow_p36
    ~~~
    
4. To ensure you can call the python functions run from
`models/research/`
    ~~~sh
    export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim`
    ~~~

5. Start training
    ~~~sh
    MODEL="20200515_RB_architecture_faster_rcnn_101-4k"
    python ./models/research/object_detection/model_main.py \
        --logstderr \
        --pipeline_config_path=./tf_config/$MODEL.config \
        --model_dir=./train_dirs/$MODEL \
        --num_train_steps=100000
    ~~~
    
6. Create another screen window and start your train monitor with 
TensorBoard. It runs on port 6006.
    ~~~sh
    tensorboard --logidr =./train_dirs/
    ~~~
    
7. Use TensorBoard to view the evolution of your training and see
how well the model detects your objects.

8. Let the training run its course or stop it whenever you prefer if
the loss plateaus.

### Testing 
1. Evaluate the model using the second config file and the last
model checkpoint:
    ~~~sh
    MODEL="20200515_RB_architecture_faster_rcnn_101-4k"
    python ./models/research/object_detection/model_main.py
        --alsologtostderr
        --run_once
        --checkpoint_dir=./train_dirs/$MODEL
        --model_dir=eval/$MODEL-eval
        --pipeline_config_path=./tf_config/$MODEL-eval.config
    ~~~
    
2. Use TensorBoard to judge how well the test set was detected.

3. Run all you test set through the model and get a JSON output
similar to `test.json`. See step 10 from the [Deploy Section](#deploy)

4. Use `scripts/statistics/confusion_matrix.py` with ground truth
and model detection JSONs to generate a confusion matrix like
this one:

<img align=center src="https://i.imgur.com/VrweDR7.png" alt="Confusion Matrix">

### Deploy
The following steps apply for deploying the model on SageMaker.

1. Export the model for tensorflow serving
    ~~~sh
    MODEL="20200515_RB_architecture_faster_rcnn_101-4k"
    python ./models/research/object_detection/export_inference_graph.py \
        --input_type=encoded_image_string_tensor \
        --pipeline_config_path=./tf_config/$MODEL.config \
        --trained_checkpoint_prefix=./train_dirs/$MODEL/model.ckpt-100000 \
        --output_directory=./serving_export/$MODEL-serving
    ~~~

2. Navigate to `./serving_export/$MODEL-serving/saved_model/` and copy
`saved_model.pb` to your computer.

3. Make a new folder, like `architecture-model`, make a version
folder inside it (like `1`) and inside the version folder copy
your `saved_model.pb`. My structure is the following:
    ~~~sh
    ─── architecture-model
        |─── 1
        |    |─── saved_model.pb  
    ~~~

4. Zip this folder structure
    ~~~sh
    tar -czvf architecture-model-v1.tar.gz architecture-model
    ~~~
    
5. On S3, make a bucket for your project and a folder for the models
inside it, like `s3://architecture-classification/models/`. Upload
`architecture-model-v1.tar.gz` here.

6. From the AWS console, navigate into SageMaker and create a notebook instance.
I'm using an `ml.t2.medium` for the notebook.

7. Inside the notebook, copy the code found in `sagemaker-deploy-testing`
and modify `architecture-predict.ipynb` to suit your needs. The notebook
deploys a model to SageMaker and tests it by sending images to it. 

    7.1 Change the colors of the rendered boxes
    
    7.2 On this line 
    ~~~py
    predictor = model.deploy(initial_instance_count=1,
                     instance_type='ml.p2.xlarge')
    ~~~
    Replace `'ml.p2.xlarge'` with `'local'` if you do not want
    to deploy a model accessible outside the notebook. Local
    deploying takes much less time.
    
    7.3 If you deployed a model locally and want to kill the process
    run the first cell to get the docker process id, copy it in
    the second cell and run it.
    
    7.4 If you want to remove an endpoint and its associated model
    from SageMaker, run the cell just after the deployment one.
    
8. In `/src/inference.py` change your classes and how you want to
handle the output from the model. In my case, I'm converting to a
JSON with the same format as the train files. The filename, width and 
height are not provided by the model and need to be set before 
the call. The handlers ignores all annotations with scores lower
than 0.2 on this line:
    ~~~py
    no_detections = len([score for score in output_dict['detection_scores'] if score * 100 > 20])
    ~~~

8. This model accepts images converted into JSONs, processing
happens in the `process_one_image` method in the notebook.

9. Run your cells, try uploading an image in `/images/` and see
how your model performs.

10. Optional. Upload your test set inside images and call
`process_one_image` for each of them, getting a JSON
for the test set detections.

### Conclusion

If everything worked correctly, your model is now deployed to
an auto-scalable SageMaker endpoint. Its URL is accesible by
navigating to SageMaker, Inference (left sidebar), Endpoints, and
clicking on your endpoint. 

The only thing you need to do is to convert your image
to base64 and process SageMaker's output.
