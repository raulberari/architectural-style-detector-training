import json
import argparse
#from google.cloud import storage
import os
import glob
from PIL import ImageOps, Image
import shutil
import io
import signal
import sys
import subprocess

parser = argparse.ArgumentParser(
    description=
    'Transforms a VoTT annotation file to a json containing a collection of objects in the visual-API format'
)
parser.add_argument('command', metavar='cmd', type=str)
parser.add_argument(
    '--source',
    type=str,
    help='The path of the source of images. Can be a google cloud bucket path.')
parser.add_argument(
    '--annotation_source',
    type=str,
    default=None,
    help=
    'The path of the annotations for the images in source. If you wish to pull them from firebase, enter \'firebase\' as the value'
)
parser.add_argument(
    '--destination',
    type=str,
    default='images/',
    help='That path to the destination folder where images should be stored.')

parser.add_argument(
    '--annotation_destination',
    type=str,
    nargs='+', action='store', dest='annotation_source',
    default=None,
    help='That path to the destination annotation file.')
parser.add_argument(
    '--label_map',
    type=str,
    default='label_map.json',
    help='Path to a json describing the mapping of labels.')
parser.add_argument(
    '--infer_boxes',
    type=bool,
    default=False,
    help=
    'If no initial boxes are provided, you can generate them by running the data through a trained model'
)
parser.add_argument(
    '--resize',
    type=bool,
    default=True,
    help=
    'If no initial boxes are provided, you can generate them by running the data through a trained model'
)
parser.add_argument(
    '--model_path',
    type=str,
    default='train_dirs/ssd_train/export/Servo/1534970687/saved_model.pb')
parser.add_argument('--export_type', type=str, default='tf_record')
args = parser.parse_args()
#gs://teleport-vision-api/uploaded_wireframes .

TEMP_FOLDER = 'download_tmp/'
MAX_SIZE = (1028, 1028)


def download_files(bucket_name, prefix):
  storage_client = storage.Client()
  bucket = storage_client.get_bucket(bucket_name)
  blobs = bucket.list_blobs(prefix=prefix)
  for blob in blobs:
    filename = blob.name.replace('/', '_')
    print("Downloading {0}".format(filename))
    blob.download_to_filename(TEMP_FOLDER + filename)


def copy_images(source,
                destination,
                annotation_source,
                annotation_destination,
                resize=True,
                infer_boxes=False):
  destination_files = sorted(glob.glob(destination + '*'))
  source_files = sorted(glob.glob(source + '*'))
  dst_file_index = 0
  new_annotations = []
  imported = 0
  infered = 0
  empty = 0
  no_files = 0
  if annotation_source is not None:
    new_annotations = load_annotations(annotation_source)
  try:
    dst_file_index = int(destination_files[-1].split('/')[-1].split('.')[0])
  except:
    dst_file_index = len(destination_files)
  for file in source_files:
    try:
      im = Image.open(file)
    except:
      print("something went wrong opening {0}".format(file))
      continue
    if resize and (im.size[0] > MAX_SIZE[0] or im.size[1] > MAX_SIZE[1]):
      im.thumbnail(MAX_SIZE, Image.ANTIALIAS)
    new_file = '{0}{1}.jpg'.format(destination, str(dst_file_index).zfill(6))
    im.save(new_file)
    no_files += 1
    image_annotation = None
    for annotation in new_annotations:
      if annotation["file"] in file:
        image_annotation = annotation
        break
    if image_annotation is None:
      if infer_boxes:
        image_annotation = infer_box(im)
        infered += 1
      else:
        image_annotation = create_empty_annotation(new_file, im.size[0],
                                                   im.size[1])
        empty += 1
      new_annotations.append(image_annotation)
    else:
      image_annotation["file"] = new_file
      imported += 1
    dst_file_index += 1
  if annotation_destination is not None:
    old_annotations = load_annotations(annotation_destination)
    new_annotations = old_annotations + new_annotations
    json.dump(
        sorted(new_annotations, key=lambda entry: entry['file']),
        open(annotation_destination, 'w'),
        indent=2)
  print("Copied {0} images to {1}.".format(no_files, destination))
  print("Imported {0} annotations from {1} to {2}".format(
      imported, annotation_source, annotation_destination))
  print("Computed {0} and added to {1}".format(infered, annotation_destination))
  print("Created {0} empty annotations and added to {1}".format(
      empty, annotation_destination))


def infer_box(file):
  raise NotImplementedError


def create_empty_annotation(file, width, height):
  return {"file": file, "width": width, "height": height, "boxes": []}


def load_annotations(file):
  loaded_annotations = json.load(open(file, 'r'))
  return loaded_annotations

def to_tf_record(image_entry):
  import tensorflow as tf
  import dataset_util

  filename = image_entry["file"]
  
  with tf.gfile.GFile(filename, 'rb') as fid:
    encoded_jpg = fid.read()
  encoded_jpg_io = io.BytesIO(encoded_jpg)
  image = Image.open(encoded_jpg_io)
  if image.format != 'JPEG':
    raise ValueError('Image format not JPEG')
  width, height = image.size
  xmin = []
  ymin = []
  xmax = []
  ymax = []
  classes = []
  classes_text = []
  for annotation in image_entry['annotations']:
    bbox = annotation["box"]
    xmin.append(bbox[1])
    xmax.append(bbox[1]+bbox[3])
    ymin.append(bbox[0])
    ymax.append(bbox[0]+bbox[2])
    classes_text.append(annotation["detectionString"].encode('utf8'))
    classes.append(annotation["detectionClass"])

  tf_example = tf.train.Example(features=tf.train.Features(feature={
      'image/height': dataset_util.int64_feature(height),
      'image/width': dataset_util.int64_feature(width),
      'image/filename': dataset_util.bytes_feature(filename.encode('utf8')),
      'image/source_id': dataset_util.bytes_feature(filename.encode('utf8')),
      'image/encoded': dataset_util.bytes_feature(encoded_jpg),
      'image/format': dataset_util.bytes_feature('jpeg'.encode('utf8')),
      'image/object/bbox/xmin': dataset_util.float_list_feature(xmin),
      'image/object/bbox/xmax': dataset_util.float_list_feature(xmax),
      'image/object/bbox/ymin': dataset_util.float_list_feature(ymin),
      'image/object/bbox/ymax': dataset_util.float_list_feature(ymax),
      'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
      'image/object/class/label': dataset_util.int64_list_feature(classes),
  }))
  return tf_example

def export_tf_records(annotaiton_file, destination):
  import tensorflow as tf
  #training_entries = []
  #for f in annotaiton_file:
  with open(annotaiton_file, 'rb') as infile:
    training_entries = json.load(infile)
          #training_entries += file_data
  writer = tf.python_io.TFRecordWriter(destination)
  for annotation in training_entries:
    try:
      tf_example = to_tf_record(annotation)
      writer.write(tf_example.SerializeToString())
    except:
      pass
  writer.close()

def to_vott_entry(image_entry):
  import tensorflow as tf
  filename = image_entry['file']
  with tf.gfile.GFile(filename, 'rb') as fid:
    encoded_jpg = fid.read()
  encoded_jpg_io = io.BytesIO(encoded_jpg)
  image = Image.open(encoded_jpg_io)
  if image.format != 'JPEG':
    raise ValueError('Image format not JPEG')
  width, height = image.size
  detections = []
  for i, bbox in enumerate(image_entry['annotations']):
    xmin_val = bbox['box'][1] * width
    xmax_val = (bbox['box'][1] + bbox['box'][3]) * width
    ymin_val = bbox['box'][0] * height
    ymax_val = (bbox['box'][0] + bbox['box'][2]) * height

    detections.append({
            'x1': xmin_val,
            'y1': ymin_val,
            'x2': xmax_val,
            'y2': ymax_val,
            'tags': [bbox['detectionString']],
            'id': i,
            'width': width,
            'height': height,
            'type': 'Rectangle',
            'name': i
    })
  return detections

def export_VoTT(annotation_file, destination):
  training_entries = json.load(open(annotation_file))
  training_entries = sorted(training_entries, key=lambda entry: entry['file'])
  frames = {}
  for i, annotation in enumerate(training_entries):
    frames[str(i)] = to_vott_entry(annotation)
  all_dets = {
    'frames': frames,
    'framerate': 1,
    'inputTags': "paragraph,label,header,button,checkbox,radiobutton,rating,toggle,dropdown,listbox,textarea,textinput,datepicker,stepperinput,slider,progressbar,image,video,container",
    'suggestiontype': 'track',
    'scd': False,
    'visitedFrames': list(range(len(training_entries))),
    'tag_colors': ["#0fffa0", "#df0da9", "#94b20b", "#001be1", "#009734", "#ff2600", "#a4003d", "#0c99cb", "#f8ff00",
                   "#8900db", "#00d475", "#d9a200", "#50db00", "#b64900", "#0003aa", "#0060ce", "#73009a", "#ce0072",
                   "#04e100"]
  }
  with open(destination + '.json', "w") as output:
    detection_json = json.dumps(all_dets, indent=4)
    output.write(detection_json)

def kill_child_at_termination(child):
  def signal_term_handler(signal, frame):
    subprocess.Popen.kill(child)
    print("KILLING ", child.pid)
    sys.exit(0)
  signal.signal(signal.SIGTERM, signal_term_handler)

def main():
  if args.command == "add":
    source = args.source
    isGS = False
    if source.split(':')[0] == "gs":
      try:
        os.mkdir(TEMP_FOLDER)
      except OSError:
        print("Creation of the directory %s failed" % TEMP_FOLDER)
      else:
        print("Successfully created the directory %s" % TEMP_FOLDER)
      isGS = True
      bucket_name = source.split('/')[2]
      folder_name = '/'.join(source.split('/')[3:]) + '/'
      download_files(bucket_name, folder_name)
      source = TEMP_FOLDER
    copy_images(
        source,
        args.destination,
        args.annotation_source,
        args.annotation_destination,
        resize=args.resize)
    if isGS:
      shutil.rmtree(source)
  if args.command == "export_data":
    if args.export_type == "tf_record":
      export_tf_records(args.annotation_source, args.destination)
    if args.export_type == "vott":
      export_VoTT(args.annotation_source, args.destination)
  if args.command == "train":
    bash_command = """python ./models/research/object_detection/model_main.py
 --logstderr --pipeline_config_path=./tf_config/ssd_mobilenetv2_wireframes.config
 --model_dir=./train_dirs/ssd_train --num_train_steps=50000 --num_eval_steps=10"""
    bash_command = bash_command.replace('\n', '')
    process = subprocess.Popen(bash_command.split())
    kill_child_at_termination(process)
    process.wait()
  if args.command == "pull":
    bash_cmd = "gsutil rsync -r gs://teleport-vision-api/dataset/{0} {0}".format(args.destination)
    process = subprocess.Popen(bash_cmd.split())
    kill_child_at_termination(process)
    process.wait()
    print("Synced folder {0}".format(args.destination))
  if args.command == "push":
    bash_cmd = "gsutil rsync -r -d {0} gs://teleport-vision-api/dataset/{0}".format(args.destination)
    process = subprocess.Popen(bash_cmd.split())
    kill_child_at_termination(process)
    process.wait()
    print("Synced folder {0}".format(args.destination))


if __name__ == "__main__":
  main()
