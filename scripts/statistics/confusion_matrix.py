import matplotlib
import tensorflow as tf
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt
import tqdm
import numpy as np
from matplotlib.colors import LogNorm, LinearSegmentedColormap

"""
    Compare the ground truth file to a test file by generating a confusion matrix using tensorflow's
    built in system.
"""

ground_truth_file = "/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/4100_dataset_test.json"
test_file = "/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Results/test-detections-100k.json"

detection_classes = [
    "American Craftsman", "American Foursquare", "Archaeminid", "Art Deco", "Art Nouveau", "Baroque", "Bauhaus",
    "Beaux Arts", "Byzantine", "Chicago School", "Colonial", "Deconstructivism", "Edwardian", "Georgian", "Gothic",
    "Greek Revival", "International Style", "Palladian", "Postmodern", "Queen Anne", "Romanesque", "Russian Revival",
    "Tudor Revival"
]

# Sort alphabetically to match the order. It's already provided that they have the same files
gt_df = pd.read_json(ground_truth_file)
test_df = pd.read_json(test_file)

gt_df = gt_df.sort_values(by=['file'])
gt_df.reset_index(inplace=True, drop=True)

test_df = test_df.sort_values(by=['file'])
test_df.reset_index(inplace=True, drop=True)

truth = []
prediction = []

excepted = []

# The confusion matrix starts with 0, our classes with 1, that's the reason for -1
for idx, item in test_df.iterrows():
    if len(item['annotations']) > 0:
        prediction.append(item['annotations'][0]['detectionClass'] - 1)
    else:
        excepted.append(item.file)

for idx, item in gt_df.iterrows():
    if item.file not in excepted:
        truth.append(item['annotations'][0]['detectionClass'] - 1)


cm = tf.contrib.metrics.confusion_matrix(truth, prediction)
with tf.Session():
    mat = tf.Tensor.eval(cm, feed_dict=None, session=None)

for idx, row in enumerate(mat):
    row_sum = sum(row)
    mat[idx] = (mat[idx] * 100) / row_sum


# Custom colors
def non_lin_cdict(steps, hexcol_array):
    cdict = {'red': (), 'green': (), 'blue': ()}
    for s, hexcol in zip(steps, hexcol_array):
        rgb = matplotlib.colors.hex2color(hexcol)
        cdict['red'] = cdict['red'] + ((s, rgb[0], rgb[0]),)
        cdict['green'] = cdict['green'] + ((s, rgb[1], rgb[1]),)
        cdict['blue'] = cdict['blue'] + ((s, rgb[2], rgb[2]),)
    return cdict


hc = np.array([[255, 245, 239], [230, 186, 176], [185, 135, 173], [120, 85, 141], [51, 40, 98], [30, 27, 84]])
hc = hc / 255
th = [0, 0.2, 0.4, 0.6, 0.8, 1]

cdict = non_lin_cdict(th, hc)
color_mapping = LinearSegmentedColormap('test', cdict)

df_cm = pd.DataFrame(mat, index=detection_classes,
                     columns=detection_classes)

plt.figure(figsize=(14, 10))
ax = sn.heatmap(df_cm, annot=True, cmap=color_mapping, fmt='g', linewidths=2)

# Set percentage symbol for non zero values and don't show anything otherwise
for t in ax.texts:
    if t.get_text() != "0":
        t.set_text(t.get_text() + "%")
    else:
        t.set_text("")
# plt.show()
plt.tight_layout()
plt.savefig('confusion-matrix.png', transparent=True)

