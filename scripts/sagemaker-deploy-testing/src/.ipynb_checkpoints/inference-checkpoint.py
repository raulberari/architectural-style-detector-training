import json
import ast
import requests
from PIL import Image
import secrets
from io import BytesIO
import base64
import numpy as np

idx_to_name = {
    1: 'american_craftsman',
    2: 'american_foursquare',
    3: 'archaeminid',
    4: 'art_deco',
    5: 'art_nouveau',
    6: 'baroque',
    7: 'bauhaus',
    8: 'beaux_arts',
    9: 'byzantine',
    10: 'chicago_school',
    11: 'colonial',
    12: 'deconstructivism',
    13: 'edwardian',
    14: 'georgian',
    15: 'gothic',
    16: 'greek_revival',
    17: 'international_style',
    18: 'palladian',
    19: 'postmodern',
    20: 'queen_anne',
    21: 'romanesque',
    22: 'russian_revival',
    23: 'tudor_revival'
}


def handler(data, context):
    """Handle request.
    Args:
        data (obj): the request data
        context (Context): an object containing request and configuration details
    Returns:
        (bytes, string): data to return to client, (optional) response content type
    """

    d = data.read().decode('utf-8')
    data_json = json.loads(d)
    
#     image = Image.open(BytesIO(base64.b64decode(data_json['instances'][0]['b64'])))
    
#     width, height = image.size
#     token = secrets.token_hex(8)
    
    # Send the image to the object-detection model
    response = requests.post(context.rest_uri, data=json.dumps(data_json))
    print("response", response)
    
    # Process the output from the object detection model
    prediction, response_content_type = _process_output(response, context)
    
    file_details = json.loads(prediction.decode('utf8'))
#     file_details['width'] = width
#     file_details['height'] = height
    
    print('received the prediction')
    
    return json.dumps(file_details).encode('utf-8'), response_content_type
    

def _process_output(data, context):
    if data.status_code != 200:
        raise ValueError(data.content.decode('utf-8'))

    response_content_type = context.accept_header
    
    prediction = data.content
    prediction = ast.literal_eval(prediction.decode('utf-8'))
    
    output_dict = prediction['predictions'][0]
    
    
    no_detections = len([score for score in output_dict['detection_scores'] if score * 100 > 20])

    annotations = []

    for idx in range(no_detections):
        annotation = {}
        annotation['score'] = float(output_dict['detection_scores'][idx])
        annotation['detectionString'] = idx_to_name[output_dict['detection_classes'][idx]]
        annotation['detectionClass'] = int(output_dict['detection_classes'][idx])

        ymin, xmin, ymax, xmax = output_dict['detection_boxes'][idx]
        ann_height = ymax - ymin
        ann_width = xmax - xmin
        box = [float(ymin), float(xmin), float(ann_height), float(ann_width)]
    
        annotation['box'] = box
    
        annotations.append(annotation)

    file_details = {}
    file_details['file'] = ''
    file_details['width'] = -1
    file_details['height'] = -1
    file_details['annotations'] = annotations
    

    prediction = json.dumps(file_details).encode('utf-8')
    return prediction, response_content_type
