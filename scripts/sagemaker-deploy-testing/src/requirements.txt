tqdm>=4.36.1
Pillow>=6.2.0
pandas>=0.25.1
numpy>=1.17.2