import os
import pandas as pd
import shutil

from tqdm import tqdm

input_json = '/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/4100_dataset_test.json'
input_folder = '/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/images/'
output_folder = '/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/test_set_images/'

test_set_filenames = []

input_df = pd.read_json(input_json)
for idx, item in input_df.iterrows():
    test_set_filenames.append(item['file'].split('/')[1])


for img in tqdm(os.listdir(input_folder)):
    if img in test_set_filenames:
        shutil.copy2(input_folder + img, output_folder + img)