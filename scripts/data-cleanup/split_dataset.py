from tqdm import tqdm
import pandas as pd
import numpy as np
import re
from sklearn.utils import shuffle

input_json = '/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/4100_dataset.json'
train_export = '/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/4100_dataset_train.json'
valid_export = '/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/4100_dataset_valid.json'
test_export = '/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/4100_dataset_test.json'

input_df = pd.read_json(input_json)


def export_to_json(exported_json, files, widths, heights, annotations_list):
    data = {
        "file": files,
        "width": widths,
        "height": heights,
        "annotations": annotations_list,
    }

    df = pd.DataFrame(data, columns=['file', 'width', 'height', 'annotations'], )
    df.to_json(exported_json, orient='records')


train_files = []
train_widths = []
train_heights = []
train_annotations = []

valid_files = []
valid_widths = []
valid_heights = []
valid_annotations = []

test_files = []
test_widths = []
test_heights = []
test_annotations = []

input_df = shuffle(input_df)
input_df.reset_index(inplace=True, drop=True)

# Since we have an irregular number of buildings in each class, ranging from 422 in queen anne to 69 in archaeminid
# We will count how many of those we have, to take 10% for each class for testing and another 10% for validation
# Everything else goes into training
style_count = {}
for idx, item in input_df.iterrows():
    if item['annotations'][0]['detectionString'] not in style_count:
        style_count[item['annotations'][0]['detectionString']] = 1

    else:
        style_count[item['annotations'][0]['detectionString']] += 1


new_style_count = {}
for style in style_count:
    style_count[style] //= 10

    new_style_count[style] = 0

print(style_count)


for idx, item in tqdm(input_df.iterrows(), total=input_df.shape[0]):
    style = item['annotations'][0]['detectionString']

    # Test set, 10% of each class
    if new_style_count[style] < style_count[style]:
        new_style_count[style] += 1

        test_files.append(item.file)
        test_widths.append(item.width)
        test_heights.append(item.height)
        test_annotations.append(item.annotations)

    # Validation set, 10% of each class
    elif new_style_count[style] < style_count[style] * 2:
        new_style_count[style] += 1

        valid_files.append(item.file)
        valid_widths.append(item.width)
        valid_heights.append(item.height)
        valid_annotations.append(item.annotations)

    # Train set, 80% of each class
    else:
        train_files.append(item.file)
        train_widths.append(item.width)
        train_heights.append(item.height)
        train_annotations.append(item.annotations)


export_to_json(train_export, train_files, train_widths, train_heights, train_annotations)
export_to_json(valid_export, valid_files, valid_widths, valid_heights, valid_annotations)
export_to_json(test_export, test_files, test_widths, test_heights, test_annotations)