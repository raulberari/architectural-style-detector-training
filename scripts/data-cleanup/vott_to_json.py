import json
from attributedict.collections import AttributeDict
from tqdm import tqdm
import pandas as pd

# all_annotations_json = '/Users/raul/Documents/Work/Programming/Teleport-Job/flowcharts/dataset/4000_dataset.json'
exported_json = '/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/4100_dataset.json'
vott_json_paths = [
    "/Users/raul/Documents/Work/Programming/Facultate/LICENTA/Dataset/Architectural-Style-Identification-export.json",
]

files = []
widths = []
heights = []
annotations_list = []

detection_classes = {
    "american_craftsman": 1,
    "american_foursquare": 2,
    "archaeminid": 3,
    "art_deco": 4,
    "art_nouveau": 5,
    "baroque": 6,
    "bauhaus": 7,
    "beaux_arts": 8,
    "byzantine": 9,
    "chicago_school": 10,
    "colonial": 11,
    "deconstructivism": 12,
    "edwardian": 13,
    "georgian": 14,
    "gothic": 15,
    "greek_revival": 16,
    "international_style": 17,
    "palladian": 18,
    "postmodern": 19,
    "queen_anne": 20,
    "romanesque": 21,
    "russian_revival": 22,
    "tudor_revival": 23,
}


def read_original_annotations(all_annotations_json, files, widths, heights, annotations_list):
    annotation_df = pd.read_json(all_annotations_json)
    for idx, item in tqdm(annotation_df.iterrows(), total=annotation_df.shape[0]):
        files.append(item['file'])
        widths.append(item['width'])
        heights.append(item['height'])
        annotations_list.append(item['annotations'])


def parse_vott_jsons(vott_json_paths, files, widths, heights, annotations_list):
    for path in tqdm(vott_json_paths):
        with open(path) as json_data:
            data = AttributeDict(json.load(json_data))

            for id in data['assets']:
                is_valid = True
                filename = 'images/' + data['assets'][id]['asset']['name']

                width = data['assets'][id]['asset']['size']['width']
                height = data['assets'][id]['asset']['size']['height']

                annotations = []

                for annotation in data['assets'][id]['regions']:
                    px_ann_top = annotation['boundingBox']['top']
                    px_ann_left = annotation['boundingBox']['left']
                    px_ann_height = annotation['boundingBox']['height']
                    px_ann_width = annotation['boundingBox']['width']

                    # create an annotation dict for a single annotation
                    annotation_dict = dict()
                    annotation_dict['score'] = 1

                    detection_string = annotation['tags'][0]

                    if detection_string not in detection_classes:
                        is_valid = False
                        continue

                    if px_ann_top == px_ann_left == px_ann_height == px_ann_width == 0:
                        continue

                    # generate the coordinates as top, left, height, width
                    top = min(1, max(0, px_ann_top / height))
                    left = min(1, max(0, px_ann_left / width))
                    ann_height = min(1, max(0, px_ann_height / height))
                    ann_width = min(1, max(0, px_ann_width / width))

                    if detection_string not in detection_classes:
                        is_valid = False
                        continue

                    if detection_string in detection_classes:
                        annotation_dict['detectionString'] = detection_string
                        annotation_dict['detectionClass'] = detection_classes[detection_string]
                        annotation_dict['box'] = [top, left, ann_height, ann_width]
                        annotations.append(annotation_dict)

                if is_valid:
                    files.append(filename)
                    widths.append(width)
                    heights.append(height)
                    annotations_list.append(annotations)


def export_to_json(exported_json, files, widths, heights, annotations_list):
    data = {
        "file": files,
        "width": widths,
        "height": heights,
        "annotations": annotations_list,
    }

    df = pd.DataFrame(data, columns=['file', 'width', 'height', 'annotations'], )
    df.to_json(exported_json, orient='records')


def main():
    # read_original_annotations(all_annotations_json, files, widths, heights, annotations_list)
    parse_vott_jsons(vott_json_paths, files, widths, heights, annotations_list)
    export_to_json(exported_json, files, widths, heights, annotations_list)


if __name__ == '__main__':
    main()
